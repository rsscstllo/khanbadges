# README #

## About This Repository ##

* An iOS application that tells Khan Academy students what the different Challenge Patches represent.  
* Version 2.0.1  
* Deployment Target: 9.0  
* Devices: Universal  
* Device Orientation: Portrait and landscape  
* Swift 3.0, Xcode 8.2.1  

## How To Set Up ##
* Download the project  
* Install [Carthage](https://github.com/Carthage/Carthage)  
* Run `carthage bootstrap --platform iOS` from Terminal in the project's main directory to download and build all dependencies specified in the `Cartfile`.  
* Double click **KhanBadges.xcodeproj**  
* Click play 😊  

## Network Testing ##
* Turn off Wi-Fi on your computer  
* Run the app in the simulator  
* Pull down to refresh  
* Pull down to refresh  
* Turn on Wi-Fi on your computer  
* Pull down to refresh  
* Pull down to refresh  
* Quit the app  
* Make sure Wi-Fi is turned on  
* Run the app in the simulator  

## Source Files ##

#### Model ####
* Badge.swift  
* ChallengePatchBadge.swift  

#### View ####
* Main.storyboard  
* BadgeHeaderTableViewCell.swift  
* BadgeTableViewCell.swift  
* BadgeDetailTableViewCell.swift  

#### Controller ####
* BadgeViewController.swift  
* BadgeDetailViewController.swift  

#### Network ####
* APIManager.swift  

#### Supporting Files ####
* AppDelegate.swift  
* Info.plist  
* Assets.xcassets  
* LaunchScreen.storyboard  

## Reference ##
* [Carthage](https://github.com/Carthage/Carthage)  
* [Unbox](https://github.com/JohnSundell/Unbox)  
* [Kingfisher](https://github.com/onevcat/Kingfisher)  
* [Khan Academy API](https://api-explorer.khanacademy.org)  
