//
//  APIManager.swift
//  KhanBadges
//
//  Created by Ross Castillo on 3/6/17.
//  Copyright © 2017 Ross Castillo. All rights reserved.
//

import Foundation
import UIKit
import Unbox

final class APIManager {
  
  static let shared = APIManager()
  
  fileprivate let challengePatchBadgeURL = URL(string: "https://www.khanacademy.org/api/v1/badges/categories?format=pretty")!
  fileprivate let badgeCategoriesURL = URL(string: "https://www.khanacademy.org/api/v1/badges?format=pretty")!
  
  func getChallengePatchBadge(completion: @escaping (ChallengePatchBadge?) -> Void) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    URLSession.shared.dataTask(with: challengePatchBadgeURL) { (data, response, error) -> Void in
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
      })
      if let badges: [ChallengePatchBadge] = self.parseJSON(data) {
        completion(badges.filter({ $0.category == 5 }).first)
      } else {
        completion(nil)
      }
    }.resume()
  }
  
  func getChallengePatchBadges(completion: @escaping ([Badge]?) -> Void) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    URLSession.shared.dataTask(with: badgeCategoriesURL) { (data, response, error) -> Void in
      DispatchQueue.main.async(execute: {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
      })
      if let badges: [Badge] = self.parseJSON(data) {
        completion(badges.filter({ $0.category == 5 }).sorted(by: { $0.name < $1.name }))
      } else {
        completion(nil)
      }
    }.resume()
  }
  
  func parseJSON<T: Unboxable>(_ data: Data?) -> [T]? {
    guard let data = data else {
      return nil
    }
    if let json = try? JSONSerialization.jsonObject(with: data, options: []) {
      print("JSON: ")
      print(json)
    }
    do {
      return try unbox(data: data) as [T]
    } catch {
      print(error)
      return nil
    }
  }
  
}
