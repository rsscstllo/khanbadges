//
//  BadgeDetailTableViewCell.swift
//  KhanBadges
//
//  Created by Ross Castillo on 12/17/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import UIKit

final class BadgeDetailTableViewCell: UITableViewCell {
  
  @IBOutlet weak var badgeIconImageView: UIImageView!
  @IBOutlet weak var badgeNameLabel: UILabel!
  @IBOutlet weak var badgeDescriptionLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    badgeDescriptionLabel.textColor = UIColor.black.withAlphaComponent(0.5)
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
}
