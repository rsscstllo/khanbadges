//
//  BadgeViewController.swift
//  KhanBadges
//
//  Created by Ross Castillo on 12/17/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import UIKit
import Kingfisher

final class BadgeViewController: UIViewController {
  fileprivate var challengePatchBadge: ChallengePatchBadge?
  fileprivate var badges: [Badge] = []
  @IBOutlet fileprivate weak var badgeTableView: UITableView!
  
  // Refresh
  
  lazy var refreshControl: UIRefreshControl = {
    let refreshControl = UIRefreshControl()
    refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
    return refreshControl
  }()
  
  func handleRefresh(_ refreshControl: UIRefreshControl) {
    if challengePatchBadge == nil {
      getChallengePatchBadge()
    }
    if badges.count == 0 {
      getChallengePatchBadges()
    }
    if challengePatchBadge != nil && badges.count > 0 {
      refreshControl.endRefreshing()
    }
  }
}

// MARK: - Life Cycle

extension BadgeViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    badgeTableView.estimatedRowHeight = 64
    refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
    badgeTableView.addSubview(refreshControl)
    badgeTableView.dataSource = self
    badgeTableView.delegate = self
    getChallengePatchBadge()
    getChallengePatchBadges()
  }
}

// MARK: - Navigation

extension BadgeViewController {
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "showBadgeDetailVC" {
      if let indexPath = badgeTableView.indexPathForSelectedRow {
        let thisBadge = badges[indexPath.row]
        let badgeDetailVC = segue.destination as! BadgeDetailViewController
        badgeDetailVC.badge = thisBadge
      }
    }
  }
}

// MARK: - API Calls

extension BadgeViewController {
  func getChallengePatchBadge() {
    APIManager.shared.getChallengePatchBadge { challengePatchBadge in
      if let challengePatchBadge = challengePatchBadge {
        self.challengePatchBadge = challengePatchBadge
        DispatchQueue.main.async {
          self.title = challengePatchBadge.name
          self.badgeTableView.reloadData()
          // Would ideally want to handle ending refreshing for this call as well, but including two separate
          // api calls in a single view controller / table view is a little uncanny to begin with.
        }
      } else {
        print("No badge found.")
      }
    }
  }
  func getChallengePatchBadges() {
    APIManager.shared.getChallengePatchBadges { badges in
      if let badges = badges {
        self.badges = badges
        DispatchQueue.main.async {
          self.badgeTableView.reloadData()
          self.refreshControl.endRefreshing()
        }
      } else {
        DispatchQueue.main.async {
          self.showAlert("No badges found. Pull down to refresh.")
        }
      }
    }
  }
  private func showAlert(_ message: String) {
    let alert = UIAlertController(title: "Error!", message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {
      action in
      self.refreshControl.endRefreshing()
    }))
    self.present(alert, animated: true, completion: nil)
  }
}

// MARK: - UITableViewDataSource

extension BadgeViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      return challengePatchBadge == nil ? 0 : 1
    } else {
      return badges.count
    }
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.section == 0 {
      let cell = badgeTableView.dequeueReusableCell(withIdentifier: "badgeHeaderCell") as! BadgeHeaderTableViewCell
      if let challengePatchBadge = challengePatchBadge {
        let url = URL(string: challengePatchBadge.icon)
        cell.badgeIconImageView.kf.setImage(with: url, placeholder: UIImage(named: "badge_placeholder")!, options: [.transition(.fade(0.8))])
        cell.badgeDescriptionLabel.text = challengePatchBadge.description
      }
      return cell
    } else {
      let cell = badgeTableView.dequeueReusableCell(withIdentifier: "badgeCell") as! BadgeTableViewCell
      let thisBadge = badges[indexPath.row]
      let url = URL(string: thisBadge.icon.compact)
      cell.badgeIconImageView.kf.setImage(with: url, placeholder: UIImage(named: "badge_placeholder")!, options: [.transition(.fade(0.8))])
      cell.badgeNameLabel.text = thisBadge.name
      return cell
    }
  }
}

// MARK: - UITableViewDelegate

extension BadgeViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if indexPath.section == 0 {
      return 200
    } else {
      return UITableViewAutomaticDimension
    }
  }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.section == 1 {
      performSegue(withIdentifier: "showBadgeDetailVC", sender: self)
    }
  }
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    if badgeTableView.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
      badgeTableView.separatorInset = UIEdgeInsets.zero
    }
    if badgeTableView.responds(to: #selector(setter: UIView.layoutMargins)) {
      badgeTableView.layoutMargins = UIEdgeInsets.zero
    }
    if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
      cell.layoutMargins = UIEdgeInsetsMake(0, 72, 0, 0)
    }
  }
}
