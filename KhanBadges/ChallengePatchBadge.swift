//
//  ChallengePatchBadge.swift
//  KhanBadges
//
//  Created by Ross Castillo on 3/18/17.
//  Copyright © 2017 Ross Castillo. All rights reserved.
//

import Foundation
import Unbox

struct ChallengePatchBadge: Unboxable {
  let category: Int
  let name: String
  let description: String
  let icon: String
  
  init(unboxer: Unboxer) throws {
    category = try unboxer.unbox(key: "category")
    name = try unboxer.unbox(key: "type_label")
    description = try unboxer.unbox(key: "description")
    icon = try unboxer.unbox(key: "large_icon_src")
  }
}
