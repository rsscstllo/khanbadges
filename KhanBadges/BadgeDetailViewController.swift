//
//  BadgeDetailViewController.swift
//  KhanBadges
//
//  Created by Ross Castillo on 12/17/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import UIKit

final class BadgeDetailViewController: UIViewController {
  var badge: Badge!
  @IBOutlet fileprivate weak var badgeDetailTableView: UITableView!
}

// MARK: Life Cycle

extension BadgeDetailViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    badgeDetailTableView.rowHeight = UITableViewAutomaticDimension
    badgeDetailTableView.estimatedRowHeight = 360
    badgeDetailTableView.dataSource = self
  }
}

// MARK: - UITableViewDataSource

extension BadgeDetailViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = badgeDetailTableView.dequeueReusableCell(withIdentifier: "badgeDetailCell") as! BadgeDetailTableViewCell
    let url = URL(string: badge.icon.large)
    cell.badgeIconImageView.kf.setImage(with: url, placeholder: UIImage(named: "badge_placeholder")!, options: [.transition(.fade(0.8))])
    cell.badgeNameLabel.text = badge.name
    cell.badgeDescriptionLabel.text = badge.description
    return cell
  }
}
