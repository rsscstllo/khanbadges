//
//  BadgeHeaderTableViewCell.swift
//  KhanBadges
//
//  Created by Ross Castillo on 12/17/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import UIKit

final class BadgeHeaderTableViewCell: UITableViewCell {
  
  @IBOutlet weak var badgeIconImageView: UIImageView!
  @IBOutlet weak var badgeDescriptionLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    contentView.backgroundColor = UIColor.groupTableViewBackground
    badgeDescriptionLabel.textColor = UIColor.black.withAlphaComponent(0.5)
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
}
