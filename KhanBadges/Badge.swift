//
//  Badge.swift
//  KhanBadges
//
//  Created by Ross Castillo on 12/17/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import Foundation
import Unbox

struct Badge: Unboxable {
  let category: Int
  let name: String
  let description: String
  let icon: Icon
  
  init(unboxer: Unboxer) throws {
    category = try unboxer.unbox(key: "badge_category")
    name = try unboxer.unbox(key: "description")
    description = try unboxer.unbox(key: "translated_safe_extended_description")
    icon = try unboxer.unbox(key: "icons")
  }
}

struct Icon: Unboxable {
  let small: String
  let compact: String
  let large: String
  
  init(unboxer: Unboxer) throws {
    small = try unboxer.unbox(key: "small")
    compact = try unboxer.unbox(key: "compact")
    large = try unboxer.unbox(key: "large")
  }
}
